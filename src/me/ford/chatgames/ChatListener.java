package me.ford.chatgames;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
	
	private final ChatGames CG;
	
	public ChatListener(ChatGames plugin) {
		CG = plugin;
	}
	
	@EventHandler
	public void onChatMessage(AsyncPlayerChatEvent event) {
		if (CG.getGame().isWaiting()) {
			if (event.getMessage().equalsIgnoreCase(CG.getGame().getAnswer()) 
					&& event.getPlayer().hasPermission("cg.play")) {
				CG.getGame().found(event.getPlayer());
			}
		}
	}
	
}
