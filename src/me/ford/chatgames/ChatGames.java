package me.ford.chatgames;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.chatgames.games.AdditionGame;
import me.ford.chatgames.games.Game;
import me.ford.chatgames.games.MultiplicationGame;

public class ChatGames extends JavaPlugin {
	static Logger LOGGER = Logger.getLogger("ChatGames");
	private Game game;
	private String gameType;
	private long runPeriod;
	private long timeOut;
	private String numberType;
	private long lmaxNumber;
	private long lminNumber;
	private double dmaxNumber;
	private double dminNumber;
	private boolean startWithPlugin;
	private List<String> winCommands = new ArrayList<String>();
	private final List<String> additionNames = Arrays.asList("addition", "add", "plus", "+");
	private final List<String> multiplyNames = Arrays.asList("multiplication", "mult", "multiply", "*", "x");
	
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new ChatListener(this), this);
		loadConfiguration();
		if (startWithPlugin) {
			game.start();
		}
		Messages.init(this);
	}
	
	private void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		setupNumberType();
		runPeriod = getConfig().getLong("games.run-period");
		setupGameType();
		timeOut = getConfig().getLong("games.timeout");
		winCommands = getConfig().getStringList("games.wincommands");
		startWithPlugin = getConfig().getBoolean("games.start-with-server");
		saveConfig();
	}
	
	private void setupGameType() {
		gameType = getConfig().getString("games.type").toLowerCase();
		if (additionNames.contains(gameType)) {
			gameType = "addition";
			game = new AdditionGame(this);
		} else {
			if (!multiplyNames.contains(gameType)) {
				LOGGER.severe("Game Type not detected! Defaulting to multiplication. (Addition also available)");
			}
			gameType = "multiplication";
			game = new MultiplicationGame(this);
		}
	}
	
	private void setupNumberType() {
		numberType = getConfig().getString("number.type");
		if (numberType.equalsIgnoreCase("float") || numberType.equalsIgnoreCase("double")) {
			numberType = "double";
			dmaxNumber = getConfig().getDouble("number.max");
			dminNumber = getConfig().getDouble("number.min");
		} else {
			if (!numberType.equalsIgnoreCase("long") && !numberType.equalsIgnoreCase("int") && !numberType.equalsIgnoreCase("integer")) {
				LOGGER.severe("Type of number not detected! Defaulting to long! (double/float also possible)");
			}
			numberType = "long";
			lmaxNumber = getConfig().getLong("number.max");
			lminNumber = getConfig().getLong("number.min");
		}
	}
	
	private boolean commandStart(CommandSender sender, String[] args) {
		// /cg start [type]
		if (args.length > 1) {
			if (multiplyNames.contains(args[1])) {
				// new multiplication
				if (game.isRunning()) {
					game.stop();
				}
				game = new MultiplicationGame(this);
			} else if (additionNames.contains(args[1])) {
				// new addition
				if (game.isRunning()) {
					game.stop();
				}
				game = new AdditionGame(this);
			}
		} 
		game.start();
		Map<String, String> map = new HashMap<String, String>();
		map.put("\\{game\\}", game.getType());
		sender.sendMessage(Messages.STARTED_GAME.get(map));
		return true;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("cg")) {
			if (args.length == 0) {
				return false;
			}
			if (args[0].equalsIgnoreCase("start")) {
				return commandStart(sender, args);
			}
		}
		return false;
	}
	
	public void broadcast(String msg) {
		for (Player op : getServer().getOnlinePlayers()) {
			op.sendMessage(msg);
		}
	}
	
	public long getTimeOut() {
		return timeOut;
	}
	
	public long getPeriod() {
		return runPeriod;
	}
	
	public String getNumberType() {
		return numberType;
	}
	
	public double getDMin() {
		return dminNumber;
	}
	
	public double getDMax() {
		return dmaxNumber;
	}
	
	public long getLMin() {
		return lminNumber;
	}
	
	public long getLMax() {
		return lmaxNumber;
	}
	
	public Game getGame() {
		return game;
	}
	
	public List<String> getWinCommands() {
		return winCommands;
	}
}
