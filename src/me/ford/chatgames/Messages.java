package me.ford.chatgames;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Messages {
	ADDITON_MESSAGE("addition", "&6What is &5{number1}&6 + &5{number2}&5?"),
	MULTIPLICATION_MESSAGE("multiplication", "&6What is &5{number1}&6 x &5{number2}&5?"),
	WIN_MESSAGE("winner", "&7{winner}&6 won with &5{answer}&6 in &5{time}&6!"),
	FAILED("failed", "&7Nobody won the game! The answer was: &5{answer}"),
	STARTED_GAME("started-game", "&6You have just started a game of &5{game}"),
	;
	
	private static FileConfiguration file;
	private final static String filepath = "messages.yml";
	private final String path;
	private String message;
	private static ChatGames CG;
	
	public static void init(ChatGames plugin) {
		CG = plugin;
		file = YamlConfiguration.loadConfiguration(new File(CG.getDataFolder(), filepath));
		file.options().copyDefaults(true);
	}
	
	/**
	 * @param path    path of message within file
	 * @param message default message
	 */
	private Messages(String path, String message) {
		this.path = path;
		this.message = message;
	}
	

	/**
	 * @return coloured message
	 */
	public String get() {
		if (file.contains(path)) { 
			// message from file
			return c(file.getString(path));
		} else {
			// default message
			return c(message);
		}
	}
	
	/**
	 * Get (coloured) message where a map is used to swap out {} tagged stuff 
	 * 
	 * @param map  map used to replace
	 * @return     the coloured message
	 */
	public String get(Map<String, String> map) {
		String msg = message;
		if (file.contains(path)) {
			msg = file.getString(path);
		}
		for (Entry<String, String> entry : map.entrySet()) {
			msg = msg.replaceAll(entry.getKey(), entry.getValue());
		}
		return c(msg);
	}
	
	/**
	 * Colours message
	 * 
	 * @param msg message to color
	 * @return    coloured message
	 */
	public String c(String msg) {
		if (msg == null) {
			return "";
		}
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
	
	protected static String getPath() {
		return filepath;
	}


}

