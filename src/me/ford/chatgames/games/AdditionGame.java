package me.ford.chatgames.games;

import me.ford.chatgames.ChatGames;
import me.ford.chatgames.Messages;

public class AdditionGame extends Game {
	
	public AdditionGame(ChatGames plugin) {
		super(plugin, "Additon");
	}
	

	@Override
	protected String getMessage() {
		return Messages.ADDITON_MESSAGE.get(map);
	}


	@Override
	protected void setAnswer() {
		if (bDouble) {
			answer = String.valueOf(dnumber1 + dnumber2);
		} else {
			answer = String.valueOf(lnumber1 + lnumber2);
		}
	}

}
