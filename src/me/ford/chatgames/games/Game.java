package me.ford.chatgames.games;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import me.ford.chatgames.ChatGames;
import me.ford.chatgames.Messages;

public abstract class Game {
	protected final String type;
	protected long start;
	protected double dnumber1;
	protected double dnumber2;
	protected long lnumber1;
	protected long lnumber2;
	protected String number1;
	protected String number2;
	protected final ChatGames CG;
	protected boolean waiting = false;
	protected boolean running = false;
	protected final boolean bDouble;
	protected String answer;
	protected Map<String, String> map = new HashMap<String, String>();
	protected BukkitTask failTask;
	protected Runnable ifFail = new Runnable() {

		@Override
		public void run() {
			if (waiting) {
				CG.broadcast(Messages.FAILED.get(map));
				waiting = false;
				playTask = CG.getServer().getScheduler().runTaskLater(CG, runPlay, CG.getPeriod() * 20L);
			}
		}
		
	};
	
	protected BukkitTask playTask;
	protected Runnable runPlay = new Runnable() {

		@Override
		public void run() {
			play();
		}
		
	};
	
	protected Game(ChatGames plugin, String gtype) {
		CG = plugin;
		bDouble = CG.getNumberType().equalsIgnoreCase("double");
		type = gtype;
	}
	
	public boolean isWaiting() {
		return waiting;
	}
	
	public boolean isRunning() {
		return running;
	}

	private void play() {
		start = System.currentTimeMillis()/1000L; // in seconds
		waiting = true;
		setNumbers();
		map.put("\\{number1\\}", number1);
		map.put("\\{number2\\}", number2);
		setAnswer();
		map.put("\\{answer\\}", answer);
		CG.broadcast(getMessage());
		failTask = CG.getServer().getScheduler().runTaskLater(CG, ifFail, 20L * CG.getTimeOut()); // cancelling
	}


	public String getWinMessage(Player winner) {
		map.put("\\{time\\}", String.valueOf(System.currentTimeMillis()/1000L - start) + "s");
		map.put("\\{winner\\}", winner.getName());
		return Messages.WIN_MESSAGE.get(map);
	}
	
	public void found(Player winner) {
		if (failTask != null) {
			failTask.cancel();
		}
		CG.broadcast(getWinMessage(winner));
		for (String cmd : CG.getWinCommands()) {
			CG.getServer().getScheduler().runTask(CG, new Runnable() {

				@Override
				public void run() {
					CG.getServer().dispatchCommand(CG.getServer().getConsoleSender(), cmd.replace("{winner}", winner.getName()));
				}
				
			});
		}
		waiting = false;
		playTask = CG.getServer().getScheduler().runTaskLater(CG, runPlay, CG.getPeriod() * 20L);
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public void start() {
		if (!running) {
			running = true;
			playTask = CG.getServer().getScheduler().runTaskLater(CG, runPlay, CG.getPeriod() * 20L);
		}
	}
	
	public void stop() {
		running = false;
		if (playTask != null) {
			playTask.cancel();
		}
		if (failTask != null) {
			failTask.cancel();
		}
	}

	protected void setNumbers() {
		if (bDouble) {
			dnumber1 = randomDouble();
			number1 = String.valueOf(dnumber1);
			dnumber2 = randomDouble();
			number2 = String.valueOf(dnumber2);			
		} else { // long
			lnumber1 = randomLong();
			number1 = String.valueOf(lnumber1);
			lnumber2 = randomLong();
			number2 = String.valueOf(lnumber2);
		}
		
	}
	
	public double randomDouble() {
		return generateRandomDoubleInRange(CG.getDMin(), CG.getDMax());
	}
	
	public double generateRandomDoubleInRange(double min, double max){
	    Random random = new Random();
	    return (max - min) * random.nextDouble() + min;
	}
	
	public long randomLong() {
		return new Long(generateRandomIntIntRange((int) (long) CG.getLMin(), (int) (long) CG.getLMax()));
	}

	public int generateRandomIntIntRange(int min, int max) {
	    Random r = new Random();
	    return r.nextInt((max - min) + 1) + min;
	}
	
	public String getType() {
		return type;
	}
	
	protected abstract void setAnswer();
	protected abstract String getMessage();
}
