# v0.0.1
Tested on spigot 1.8.8

`/cg start` to start manually
`/cg start multiplication` to start a new game of multiplication (cancels the last game)
`/cg start addition` to start a new game of addition (cancels the last game)

### Commands and permissions
```
Command             	Permission              	Description                             
cg                  	cg.main                 	ChatGames command                       
```
### Extra permissions
```
Permission              	Children                                        	Descrpition                             
cg.main                 	None                                            	Allow using the main command            
cg.play                 	None                                            	Allow paying the game      
```

### Config

```
number:                     
  max: 2000                 # maximum number appearing in game
  min: 100                  # minimum number appearing in game
  type: integer             # type of number (integer/float)
games:   
  run-period: 30            # time before a new game (in seconds)
  type: Addition            # type of game (Addition or Multiplication)
  timeout: 15               # time available for solving
  wincommands:              # commands run when when someone eins
  - tell {winner} You won!  
  - eco give {winner} 10
  start-with-server: true   # whether or not to start with the server
```